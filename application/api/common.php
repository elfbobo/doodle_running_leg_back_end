<?php

function ajaxReturn($data){
	exit(json($data)->getContent());
}

/**
 * 发送短信记录
 * @param status Text
 * @param array(status,ip,mobile,content,time)
 * @return bool
 */
function sms_log($data){
	return db('sms_log')->insert($data);
}


/*
 function rad($d){
 return $d * 3.1415926535898 / 180.0;
 }
 function GetDistance($lat1, $lng1, $lat2, $lng2){
 $EARTH_RADIUS = 6378.137;
 $radLat1 = rad($lat1);
 //echo $radLat1;
 $radLat2 = rad($lat2);
 $a = $radLat1 - $radLat2;
 $b = rad($lng1) - rad($lng2);
 $s = 2 * asin(sqrt(pow(sin($a/2),2) +
 cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));
 $s = $s *$EARTH_RADIUS;
 $s = round($s * 10000) / 10000;
 return $s;
 }
 */

// p(GetDistance(113.32212,24.147166,113.323737,23.14602));
