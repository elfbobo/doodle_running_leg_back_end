<?php
namespace app\api\controller;

class Income extends Common {
	private $token;
	private $userinfo;
	// public function faaa(){
	// 	$aa = create_salt();
	// 	p($aa);
	// 	p(optimizedSaltPwd(123456,$aa));
	// }
	public function f_my_income(){
		$this->is_validate();
		$model = db('delivery_order');
		$user_id = $this->userinfo['id'];
		$orders = $model->where(array('userId'=>$user_id,'iscount'=>1))->select();
		$all_money = 0; // 总资产
		$stay_money = 0; // 待到账
		foreach ($orders as $order) {		
			if ($order['status']==3 || (($order['status']==1 || $order['status']==2) && $order['iscancel']==1)) {
				$all_money += $order['deliverMoney'];
			}elseif ($order['status']==2) {
				$stay_money += $order['deliverMoney'];
			}
		}
		
		// 提现总金额
		$applys = db('apply')->where('userId',$user_id)->select();
		$apply_price = 0;
		foreach ($applys as $apply) {
			if ($apply['status']==0 || $apply['status']==1 || $apply['status']==2) {
				$apply_price += $apply['apply_price'];
			}
		}
		// 账户余额
		$balance = $all_money - $apply_price;
		
		$all_money = number_format($all_money, 2, '.', '');
		$stay_money = number_format($stay_money, 2, '.', '');
		$balance = number_format($balance, 2, '.', '');
		db('user')->where('id',$user_id)->update(['money'=>$balance]);
		ajaxReturn(array(
				'status' => 'success',
				'data' => array(
						'all_money' => $all_money,
						'stay_money' => $stay_money,
						'balance' => $balance
						)
		));
		
	}

	private function is_validate(){
		if (!request()->isPost()) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$token = $this->request->param('token');
		if ($token != config('token')) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$user_id = $this->request->post('userId');
		$user_id = _encrypt($user_id,'DECODE');
		$userinfo = db('user')->where(array('id'=>$user_id,'check_status'=>1))->find();
		if (empty($userinfo)) {
			ajaxReturn(array('status' => 'error','msg' => '您未认证，请先认证。'));
		}
		$this->token = $token;
		$this->userinfo = $userinfo;
	}
}
?>