<?php
namespace app\api\controller;
use think\Db;

class Test  {
		private $db = null;
	private $config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $orderField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}
    public function index(){
    	// print_r(config('database.db_config'));
     //    echo 'api index()';
     //    print_r($this->db);
     action('api/Runpush/runJpush');
    }

	public function filterOrders(){
		$jsonstr = '{"list":[{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T15:03:48","activeAt":"2018-06-26T15:03:48","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894966573,"skuId":516805754796,"name":"宫保鸡丁-0","categoryId":1,"price":0.02,"quantity":2,"total":0.04,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844155660}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.02,"quantity":1,"total":0.02,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025247975466031339","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":6,"status":"invalid","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.07,"originalPrice":0.07,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.02,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false},{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T15:03:13","activeAt":"2018-06-26T15:03:13","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894971579,"skuId":516810921900,"name":"宫保鸡丁-6","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844167993}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.01,"quantity":1,"total":0.01,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025247874727237867","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":5,"status":"valid","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.04,"originalPrice":0.04,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.01,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false},{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T14:54:18","activeAt":"2018-06-26T14:54:18","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894958490,"skuId":516797484972,"name":"宫保鸡丁-8","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844150732}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.01,"quantity":1,"total":0.01,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025247680765843691","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":4,"status":"invalid","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.04,"originalPrice":0.04,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.01,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false},{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T14:51:25","activeAt":"2018-06-26T14:51:25","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894938590,"skuId":516777120684,"name":"宫保鸡丁-3","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844140746}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.01,"quantity":1,"total":0.01,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025247533713545451","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":3,"status":"settled","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.04,"originalPrice":0.04,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.01,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false},{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T14:47:31","activeAt":"2018-06-26T14:47:31","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894966573,"skuId":516805754796,"name":"宫保鸡丁-0","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844155660},{"id":894962542,"skuId":516801634220,"name":"宫保鸡丁-1","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844163229},{"id":894962544,"skuId":516801636268,"name":"宫保鸡丁-7","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844163231}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.03,"quantity":1,"total":0.03,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025247407683098859","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":2,"status":"settled","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.1,"originalPrice":0.1,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.03,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false},{"address":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","createdAt":"2018-06-26T10:00:41","activeAt":"2018-06-26T10:00:41","deliverFee":0.01,"vipDeliveryFeeDiscount":0,"deliverTime":null,"description":"","groups":[{"name":"1号篮子","type":"normal","items":[{"id":894971579,"skuId":516810921900,"name":"宫保鸡丁-6","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844167993},{"id":894974920,"skuId":516814308268,"name":"宫保鸡丁-4","categoryId":1,"price":0.02,"quantity":1,"total":0.02,"newSpecs":[],"attributes":[],"extendCode":"","barCode":"","weight":1,"userPrice":0,"shopPrice":0,"vfoodId":844176470}]},{"name":"其它费用","type":"extra","items":[{"id":-70000,"skuId":-1,"name":"餐盒","categoryId":102,"price":0.02,"quantity":1,"total":0.02,"newSpecs":null,"attributes":null,"extendCode":"","barCode":"","weight":null,"userPrice":0,"shopPrice":0,"vfoodId":0}]}],"invoice":null,"book":false,"onlinePaid":true,"id":"3025222981994371307","phoneList":["18813974748"],"shopId":163785662,"openId":"","shopName":"三只小鸡测试店铺","daySn":1,"status":"settled","refundStatus":"noRefund","userId":1731703593,"userIdStr":"1731703593","totalPrice":0.07,"originalPrice":0.07,"consignee":"好**","deliveryGeo":"121.4840891,31.67313698","deliveryPoiAddress":"上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)","invoiced":false,"income":0,"serviceRate":0.08,"serviceFee":-1,"hongbao":0,"packageFee":0.02,"activityTotal":0,"shopPart":0,"elemePart":0,"downgraded":false,"secretPhoneExpireTime":null,"orderActivities":[],"invoiceType":null,"taxpayerId":"","coldBoxFee":0,"cancelOrderDescription":null,"cancelOrderCreatedAt":null,"orderCommissions":[],"baiduWaimai":false}],"total":6}';
		$arr = json_decode($jsonstr,true);
		$list = $arr['list'];
		if(!empty($list)){
			$filterData = [];
			foreach($list as $key => $val){
				if($val['status'] == 'valid'){
					$filterData[] = $list[$key];
				}
			}
		}
		print_r($arr['list']);
	}

    
}


// Array
// (
//     [0] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T15:03:48
//             [activeAt] => 2018-06-26T15:03:48
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894966573
//                                             [skuId] => 516805754796
//                                             [name] => 宫保鸡丁-0
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 2
//                                             [total] => 0.04
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844155660
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025247975466031339
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 6
//             [status] => invalid
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.07
//             [originalPrice] => 0.07
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.02
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

//     [1] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T15:03:13
//             [activeAt] => 2018-06-26T15:03:13
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894971579
//                                             [skuId] => 516810921900
//                                             [name] => 宫保鸡丁-6
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844167993
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.01
//                                             [quantity] => 1
//                                             [total] => 0.01
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025247874727237867
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 5
//             [status] => valid
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.04
//             [originalPrice] => 0.04
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.01
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

//     [2] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T14:54:18
//             [activeAt] => 2018-06-26T14:54:18
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894958490
//                                             [skuId] => 516797484972
//                                             [name] => 宫保鸡丁-8
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844150732
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.01
//                                             [quantity] => 1
//                                             [total] => 0.01
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025247680765843691
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 4
//             [status] => invalid
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.04
//             [originalPrice] => 0.04
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.01
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

//     [3] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T14:51:25
//             [activeAt] => 2018-06-26T14:51:25
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894938590
//                                             [skuId] => 516777120684
//                                             [name] => 宫保鸡丁-3
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844140746
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.01
//                                             [quantity] => 1
//                                             [total] => 0.01
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025247533713545451
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 3
//             [status] => settled
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.04
//             [originalPrice] => 0.04
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.01
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

//     [4] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T14:47:31
//             [activeAt] => 2018-06-26T14:47:31
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894966573
//                                             [skuId] => 516805754796
//                                             [name] => 宫保鸡丁-0
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844155660
//                                         )

//                                     [1] => Array
//                                         (
//                                             [id] => 894962542
//                                             [skuId] => 516801634220
//                                             [name] => 宫保鸡丁-1
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844163229
//                                         )

//                                     [2] => Array
//                                         (
//                                             [id] => 894962544
//                                             [skuId] => 516801636268
//                                             [name] => 宫保鸡丁-7
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844163231
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.03
//                                             [quantity] => 1
//                                             [total] => 0.03
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025247407683098859
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 2
//             [status] => settled
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.1
//             [originalPrice] => 0.1
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.03
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

//     [5] => Array
//         (
//             [address] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [createdAt] => 2018-06-26T10:00:41
//             [activeAt] => 2018-06-26T10:00:41
//             [deliverFee] => 0.01
//             [vipDeliveryFeeDiscount] => 0
//             [deliverTime] => 
//             [description] => 
//             [groups] => Array
//                 (
//                     [0] => Array
//                         (
//                             [name] => 1号篮子
//                             [type] => normal
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 894971579
//                                             [skuId] => 516810921900
//                                             [name] => 宫保鸡丁-6
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844167993
//                                         )

//                                     [1] => Array
//                                         (
//                                             [id] => 894974920
//                                             [skuId] => 516814308268
//                                             [name] => 宫保鸡丁-4
//                                             [categoryId] => 1
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => Array
//                                                 (
//                                                 )

//                                             [attributes] => Array
//                                                 (
//                                                 )

//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 1
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 844176470
//                                         )

//                                 )

//                         )

//                     [1] => Array
//                         (
//                             [name] => 其它费用
//                             [type] => extra
//                             [items] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => -70000
//                                             [skuId] => -1
//                                             [name] => 餐盒
//                                             [categoryId] => 102
//                                             [price] => 0.02
//                                             [quantity] => 1
//                                             [total] => 0.02
//                                             [newSpecs] => 
//                                             [attributes] => 
//                                             [extendCode] => 
//                                             [barCode] => 
//                                             [weight] => 
//                                             [userPrice] => 0
//                                             [shopPrice] => 0
//                                             [vfoodId] => 0
//                                         )

//                                 )

//                         )

//                 )

//             [invoice] => 
//             [book] => 
//             [onlinePaid] => 1
//             [id] => 3025222981994371307
//             [phoneList] => Array
//                 (
//                     [0] => 18813974748
//                 )

//             [shopId] => 163785662
//             [openId] => 
//             [shopName] => 三只小鸡测试店铺
//             [daySn] => 1
//             [status] => settled
//             [refundStatus] => noRefund
//             [userId] => 1731703593
//             [userIdStr] => 1731703593
//             [totalPrice] => 0.07
//             [originalPrice] => 0.07
//             [consignee] => 好**
//             [deliveryGeo] => 121.4840891,31.67313698
//             [deliveryPoiAddress] => 上海滨江度假中心北沿公路2079号(东平国家森林公园东大门对面)
//             [invoiced] => 
//             [income] => 0
//             [serviceRate] => 0.08
//             [serviceFee] => -1
//             [hongbao] => 0
//             [packageFee] => 0.02
//             [activityTotal] => 0
//             [shopPart] => 0
//             [elemePart] => 0
//             [downgraded] => 
//             [secretPhoneExpireTime] => 
//             [orderActivities] => Array
//                 (
//                 )

//             [invoiceType] => 
//             [taxpayerId] => 
//             [coldBoxFee] => 0
//             [cancelOrderDescription] => 
//             [cancelOrderCreatedAt] => 
//             [orderCommissions] => Array
//                 (
//                 )

//             [baiduWaimai] => 
//         )

// )