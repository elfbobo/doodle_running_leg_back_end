<?php
namespace app\admin\controller;

class Coupon extends Common {
	
	public function index(){
		$type = intval($this->request->param('type'));
		$where = '';
		$time = time();
		if ($type == 1){
			$where = "endtime >= {$time}";
		}elseif ($type == 2){
			$where = "endtime < {$time}";
		}
		$lists = db('coupon')->where($where)->order('time desc')->paginate(config('page_size'),false,array('query' => array('type' => $type)));
		$this->assign('lists',$lists);
		$this->assign('page',$lists->render());
		return $this->fetch();
	}
	
	public function senduser(){
		$id = intval($this->request->param('id'));
		$lists = db('coupon_use')->where(array('coupon_id' => $id))->order('time desc')->paginate(config('page_size'));
		$this->assign('lists',$lists);
		$this->assign('page',$lists->render());
		return $this->fetch();
	}
	
	public function uselist(){
		$lists = db('coupon_use')->order('time desc')->paginate(config('page_size'));
		$this->assign('lists',$lists);
		$this->assign('page',$lists->render());
		return $this->fetch();
	}
	
	public function add(){
		$this->db = \Think\Db::connect(config('database')['db_config']);
		$users = $this->db->query("select userId,loginName,userName,userPhone,userEmail from oto_users");
		$this->assign('users',$users);
		return $this->fetch();
	}
	
	public function edit(){
		$id = intval(input('id'));
		$data = db('issue')->where(array('id' => $id))->find();
		if (empty($data)) $this->error('数据不存在');
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function delete(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('issue')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function run_edit(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = array(
			'id' => input('post.id'),
			'title' => input('post.title'),
			'type' => input('post.type'),
		);
		if (empty($data['title'])) $this->ajaxReturn(array('code' => 0,'msg' => '标题不能为空'));
		if (db('issue')->update($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('index'),'msg' => '编辑成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑成功'));
		}
	}
	
	public function run_add(){
		if (!request()->isAjax()) $this->error('操作失败');
		$post = request()->post();
		$data = array(
			'title' => $post['title'],
			'money' => $post['money'],
			'type' => $post['type'],
			'endtime' => strtotime($post['endtime']),
			'time' => time()
		);
		if (empty($data['title'])) $this->ajaxReturn(array('code' => 0,'msg' => '请输入标题'));
		if (empty($data['money'])) $this->ajaxReturn(array('code' => 0,'msg' => '请输入金额'));
		if (empty($data['endtime'])) $this->ajaxReturn(array('code' => 0,'msg' => '请输入失效时间'));
		if ($insert_id = db('coupon')->insert($data,false,true)){
			
			if ($post['type'] == 1){
				$this->db = \Think\Db::connect(config('database')['db_config']);
				$users = $this->db->query("select userId,loginName,userName,userPhone,userEmail from oto_users");
				$addAll = array();
				foreach ($users as $v){
					$addAll[] = array(
							'uid' => $v['userId'],
							'money' => $data['money'],
							'coupon_id' => $insert_id,
							'time' => time(),
							'title' => $data['title'],
							'status' => 0
					);
				}

				db('coupon_use')->insertAll($addAll);
			}else{
				if (!empty($post['uid'])){
					$addAll = array();
					foreach ($post['uid'] as $v){
						$addAll[] = array(
							'uid' => $v,
							'money' => $data['money'],
							'coupon_id' => $insert_id,
							'time' => time(),
							'title' => $data['title'],
							'status' => 0
						);
					}
					db('coupon_use')->insertAll($addAll);
				}
			}
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('index'),'msg' => '发布成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '发布失败'));
		}
	}
	
}