<?php
namespace app\admin\controller;

class Weblog extends Common {
	
	public function index(){
		$lists = db('web_log')->order('otime desc')->paginate(config('page_size'));
		$this->assign('page',$lists->render());
		$this->assign('lists',$lists);
		return $this->fetch();
	}
	
	public function delete(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('web_log')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function delete_all(){
		if (!request()->isAjax()) $this->error('操作失败');
		$ids = input('post.tables');
		$ids = explode('|', rtrim($ids,'|'));
		if (!empty($ids) && db('web_log')->where(array('id' => array('IN',$ids)))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}
		$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
	}
	
	public function clear_all(){
		if (!request()->isAjax()) $this->error('操作失败');
		if (db()->execute("delete from ".config('database.prefix')."web_log where 1")){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '清空完成'));
		}
		$this->ajaxReturn(array('code' => 0,'msg' => '清空失败'));
	}
	
}