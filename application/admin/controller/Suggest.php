<?php 
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 反馈建议控制器
*/
class Suggest extends Common
{
	private $db = null;
	private $config = '';
	private $default_config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $userField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->default_config = config('database');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	/**
	 * [index 反馈建议列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = $this->db->name('suggest');
		$listobj = $m->alias('s')
		->join("users u","u.userId=s.userId","left")
		->where('s.isDel=0')
		->field('s.*,u.userName,s.id as sid')
		->order('s.createTime desc')
		->paginate(10);
		$page = $listobj->render();
		$listarr = $listobj->toArray();
		$list = $listarr['data'];
		// dump($list);
		$this->assign('page',$page);
		$this->assign('list',$list);
		return $this->fetch('list');
	}

	/**
	 * [del 删除建议]
	 * @return [json] [description]
	 */
	public function del(){
		$m = $this->db->name('suggest');
		$id = input('id/d');
		$data['isDel'] = 1;
		if($m->where(['id'=>$id])->update($data)){
			$result = ['status'=>1,'msg'=>'删除成功','id'=>$id];
		}else{
			$result = ['status'=>0,'msg'=>'删除失败'];
		}
		return json($result);
	}
}