<?php
namespace app\admin\controller;

class Drawmoney extends Common {
	
	public function index(){
		$lists = db('apply')->order('time desc')->paginate(config('PAGE_SIZE'));
		$this->assign('lists',$lists);
		$this->assign('page',$lists->render());
		return $this->fetch();
	}
	
	public function finish(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		$db = db('apply');
		if ($db->where(array('id' => $id))->setField('status',2)){
			$data = $db->where(array('id' => $id))->find();
			db('water')->insert(array(
					'uid' => $data['userId'],
					'orderid' => $data['orderId'],
					'type' => 2,
					'money' => $data['apply_price'],
					'time' => time()
			));
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '处理成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '处理失败'));
		}
	}
	
	public function action_work(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('apply')->where(array('id' => $id))->setField('status',1)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '处理成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '处理失败'));
		}
	}
	
	public function closes(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('apply')->where(array('id' => $id))->setField('status',3)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '处理成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '处理失败'));
		}
	}
	
}