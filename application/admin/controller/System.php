<?php
namespace app\admin\controller;
class System extends Common {
	
	public function setting(){
		$config = db('system')->select();
		foreach ($config as $key => $value){
			$config[$value['name']] = $value['value'];
			unset($config[$key]);
		}
		$this->assign('config',$config);
		return $this->fetch();
	}
	
	public function edit_setting_run(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = request()->post();
		$model = db('system');
		foreach ($data as $key => $value){
			$model->where(array('name' => $key))->setField('value',$value);
		}
		if (!isset($data['open_site'])){
			$model->where(array('name' => 'open_site'))->setField('value',0);
		}
		if (!isset($data['open_app'])){
			$model->where(array('name' => 'open_app'))->setField('value',0);
		}
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function email(){
		$model = db('system');
		$email = $model->where(array('name' => 'email_config'))->find();
		$email['value'] = unserialize($email['value']);
		$this->assign('email',$email);
		$this->assign($email['value']);
		$this->assign('email_reg_tpl',$model->where(array('name' => 'email_tpl_reg'))->find());
		$this->assign('email_find_tpl',$model->where(array('name' => 'email_tpl_find'))->find());
		return $this->fetch();
	}
	
	public function edit_email_run(){
		if (!request()->isAjax()) $this->error('操作失败');		
		$data = array(
			'smtp_host' => input('post.smtp_host'),
			'smtp_port' => intval(input('post.smtp_port')),
			'smtp_user' => input('post.smtp_user'),
			'smtp_pass' => input('post.smtp_pass'),
			'from_email' => input('post.from_email'),
			'from_name' => input('post.from_name'),
			'reply_email' => input('post.reply_email'),
			'reply_name' => input('post.reply_name'),
			'open_email' => input('post.open_email'),
		);
		db('system')->where(array('name' => 'email_config'))->update(array('value' => serialize($data)));
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function edittpl_email_run(){
		if (!request()->isAjax()) $this->error('操作失败');
		$model = db('system');
		$model->where(array('name' => 'email_tpl_reg'))->update(array('value' => input('post.email_tpl_reg')));
		$model->where(array('name' => 'email_tpl_find'))->update(array('value' => input('post.email_tpl_find')));
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function test_email(){
		if (!request()->isAjax()) $this->error('操作失败');
		$email = input('post.email');
		if(SendMail($email, '测试邮件发送', '邮件内容') == true){
			$this->ajaxReturn(array('code' => 1,'msg' => '发送成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '发送失败'));
		}
	}
	
	public function sms(){
		$model = db('system');
		$sms = $model->where(array('name' => 'sms_config'))->find();
		$sms['value'] = unserialize($sms['value']);
		$this->assign('sms',$sms);
		$smsApi = new \Sms();
		$info = $smsApi->overage();
		if (is_array($info)){
			$text = $info['msg'];
		}else{
			if ($info->returnstatus == 'Sucess'){
				$text = '总条数：'.$info->sendTotal.'，剩余'.$info->overage.'条';
			}else{
				$text = $info->message;
			}
		}
		$this->assign('text',$text);
		$this->assign($sms['value']);
		$this->assign('sms_check_tpl',$model->where(array('name' => 'sms_check_tpl'))->find());
		$this->assign('sms_find_tpl',$model->where(array('name' => 'sms_find_tpl'))->find());
		return $this->fetch();
	}
	
	public function edit_sms_run(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = array(
			'open_sms' => input('post.open_sms'),
			'accountsid' => input('post.accountsid'),
			'authtoken' => input('post.authtoken'),
			'appid' => input('post.appid'),
			'mobile' => input('post.mobile'),
			'uid' => input('post.uid'),
			'mid' => input('post.mid'),
			'mpass' => input('post.mpass'),
			'mqianming' => input('post.mqianming'),
		);
		db('system')->where(array('name' => 'sms_config'))->update(array('value' => serialize($data)));
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function edittpl_sms_run(){
		if (!request()->isAjax()) $this->error('操作失败');
		$model = db('system');
		$model->where(array('name' => 'sms_check_tpl'))->update(array('value' => input('post.sms_check_tpl')));
		$model->where(array('name' => 'sms_find_tpl'))->update(array('value' => input('post.sms_find_tpl')));
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function test_mobile(){
		if (!request()->isAjax()) $this->error('操作失败');
		$mobile= input('post.mobile');
		$content = input('post.content');
		$info = SendMobile($mobile, $content);
		if($info['status'] == 1){
			$this->ajaxReturn(array('code' => 1,'msg' => '发送成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => $info['msg']));
		}
	}
	
	public function payment(){
		return $this->fetch();
	}
	
	
	
}