<?php
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 文章控制器
*/
class Articles extends Common
{
	private $db = null;
	private $config = '';
	private $default_config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $userField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->default_config = config('database');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}
	/**
	 * [index 文章列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = $this->db->name('articles');
		$articleTitle = input('articleTitle','');
		$articleTitle = input('articleTitle');
		$pageParam    = ['query' =>[]];
        if ($articleTitle!='') {
        	$m->where('articleTitle', 'like', "%{$articleTitle}%");
            $pageParam['query']['a.articleTitle'] = $articleTitle;
        }
        $listobj = $m
            ->alias("a")
            ->field('a.*,c.catId,c.catType,c.catFlag,c.catName,c.catSort')
            ->join('article_cats c','a.catId=c.catId')
            ->join('huadoupaotuibase.pt_admin u','u.id = a.staffId')
            ->field('u.truename as staffName')
            // ->fetchSql()
            // ->select(false);
            ->paginate(15,false,$pageParam);
		$page = $listobj->render();
		$listarr = $listobj->toArray();
		$list = $listarr['data'];
		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('articleTitle', input('articleTitle',''));
		return $this->fetch('list');
	}

	/**
	 * [toEdit 跳转]
	 * @return [type] [description]
	 */
	public function toedit(){
		$m = $this->db->name('articles');
		// $catLists = $m->where('catFlag=1')->select();
		$id = input('id',0);
		$catLists = $this->getCatLists(); 
    	$this->assign('catLists',$catLists);
    	if($id>0){
    		$item = $m->where('articleId='.$id)->find();
    		$this->assign('item',$item);
    		// $this->assign('city',$city);
    		return $this->fetch('edit');
    	}else{
    		// $pid = input('pid/d');
			return $this->fetch('add');
    	}
	}

	/**
	 * [add 添加文章]
	 */
	public function add(){
		$m = $this->db->name('articles');
		$data = array();
		$data["catId"] = (int)input("catId");
		$data["articleTitle"] = input("articleTitle");
		$data["isShow"] = (int)input("isShow",0);
		$data["articleContent"] = input("articleContent");
		$data["articleKey"] = input("articleKey");
		$data["staffId"] = (int)session('handler_id');
		$data["createTime"] = date('Y-m-d H:i:s');
	    if($m->insert($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('articles/index'),'msg' => '添加成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articles/index'),'msg' => '添加失败');
		}
		return json($result);
	}

	/**
	 * [edit 修改文章]
	 * @return [type] [description]
	 */
	public function edit(){
		$id = input('id');
		$m = $this->db->name('articles');
		$data = array();
		$data["catId"] = (int)input("catId");
		$data["articleTitle"] = input("articleTitle");
		$data["isShow"] = (int)input("isShow",0);
		$data["articleContent"] = input("articleContent");
		$data["articleKey"] = input("articleKey");
		$data["staffId"] = (int)session('handler_id');
		$data["createTime"] = date('Y-m-d H:i:s');
	    if($m->where('articleId='.$id)->update($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('articles/index'),'msg' => '修改成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articles/index'),'msg' => '修改失败');
		}
		return json($result);
	}

	/**
	 * [toggleIsShow 切换状态]
	 * @return [json ] [description]
	 */
	public function toggleIsShow(){
		$id = input('id/d');
		$status = input('status/d');
		$data = ['isShow'=>$status];
		// dump($this->db->name('articles'));
		$res = $this->db->name('articles')->where(['articleId'=>$id])->update($data);
		if($res){
			$result = ['status'=>1,'msg'=>'切换成功','id'=>$id,'state'=>$status];
		}else{
			$result = ['status'=>0,'msg'=>'切换失败'];
		}
		return json($result);
	}

	/**
	 * [del 删除文章]
	 * @return [json] [description]
	 */
	public function del(){
		$m = $this->db->name('articles');
		$id = input('id/d');
		if($m->where(['articleId'=>$id])->delete()){
			$result = array('code' => 1,'reload' => 1,'url' => url('articles/index'),'msg' => '删除成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articles/index'),'msg' => '删除失败');
		}
		return json($result);
	}

	/**
	  * 获取所有的类别，并且添加层级
	  */
	 protected function getCatLists(){	
	 	$m = $this->db->name('article_cats');
	 	$sql = "select * from ".$this->oto_db_prefix."article_cats where catFlag = 1 order by catSort asc";
	 	$catList = $m->query($sql);
	 	if ($catList !== false) {	 		
	 		$catList = self::unlimitedForLevel($catList);	 		
	 	}
	 	return $catList;
	 }

	 Static Public function unlimitedForLevel($cate,$html='&nbsp;&nbsp;',$parentId=0,$level=0){
		$arr = array();
		foreach ($cate as $v) {
			if ($v['parentId'] == $parentId) {
				$v['level'] = $level + 1;
				$html2 = $level==0 ? '' : '|--';//生成目录|--
				$v['html'] = str_repeat($html,$level).$html2;
				$v['catName'] = $v['html'].$v['catName'];
				$arr[]=$v;
				$arr = array_merge($arr,self::unlimitedForLevel($cate,$html,$v['catId'],$level + 1));
			}
		}
		return $arr;
	}

}