<?php
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 文章分类控制器
*/
class Articlecats extends Common
{
	private $db = null;
	private $config = '';
	private $default_config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $userField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->default_config = config('database');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	/**
	 * [index 文章分类列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = $this->db->name('article_cats');
		$list = $this->catLists();
		// $list = $m->where('parentId=0 and catFlag=1')->order('catId asc,catSort desc')->select();
		// dump($list);
		$this->assign('list',$list);
		return $this->fetch('list');
	}

	/**
	 * 跳到新增/编辑页面
	 */
	public function toEdit(){
		//获取地区信息
		$m = $this->db->name('article_cats');
		$catLists = $m->where('catFlag=1')->select();
		$id = input('id',0);
    	if($id>0){
    		$item = $m->where('catId='.$id)->find();
    		$this->assign('item',$item);
    		// $this->assign('city',$city);
    		return $this->fetch('edit');
    	}else{
    		$pid = input('pid/d');
    		$this->assign('pid',$pid);
    		$this->assign('catLists',$catLists);
			return $this->fetch('add');
    	}
	}

	/**
	 * [add 新增分类]
	 */
	public function add(){
		$pid = input('pid/d');
		// dump($this->request->param());exit;
		if($pid>0){
			$data['parentId'] = $pid;
		}else{
			$data['parentId'] = 0;
		}
		$m = $this->db->name('article_cats');
		$data["catName"] = input("catName");
		$data["catFlag"] = 1;
		$data["isShow"] = input("isShow/d");
		$data["catType"] = 0;
		$data["catSort"] = input("catSort",0);
		// dump($data);exit;
		if($m->insert($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('articlecats/index'),'msg' => '添加成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articlecats/index'),'msg' => '添加失败');
		}
		return json($result);
	}

	/**
	 * [edit 修改文章分类]
	 * @return [json] [description]
	 */
	public function edit(){
		$m = $this->db->name('article_cats');
		$id = input('id/d');
		$data["catName"] = input("catName");
		$data["isShow"] = input("isShow/d");
		$data["catSort"] = input("catSort",0);
		$res = $m->where(['catId'=>$id])->update($data);
		if($res){
			$result = array('code' => 1,'reload' => 1,'url' => url('articlecats/index'),'msg' => '修改成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articlecats/index'),'msg' => '修改失败');
		}
		return json($result);
	}

	/**
	 * [del 删除文章分类]
	 * @return [json] [description]
	 */
	public function del(){
		$m = $this->db->name('article_cats');
		$id = input('id/d');
		if($m->where(['catId'=>$id])->delete()){
			$result = array('code' => 1,'reload' => 1,'url' => url('articlecats/index'),'msg' => '删除成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('articlecats/index'),'msg' => '删除失败');
		}
		return json($result);
	}

	/**
	  * [CatLists 父子菜单列表]
	  */
	 protected function CatLists(){
	 	$m = $this->db->name('article_cats');
	 	$sql = "select * from ".$this->oto_db_prefix."article_cats where catFlag = 1 order by catSort asc";
	 	$catList = $m->query($sql);
	 	if ($catList !== false) {	 		
			$catList = self::getChildList($catList);	 		
	 	}
	 	return $catList;
	 }

	 /**
	  * [getChildList 获取菜单树]
	  * @param  [type]  $cate     [description]
	  * @param  integer $parentId [description]
	  * @param  integer $level    [description]
	  * @return [type]            [description]
	  */
	 static protected function getChildList($cate,$parentId=0,$level=0){
	 	$arr = array();
	 	foreach ($cate as $k => $v) {
			if ($v['parentId'] == $parentId) {
				$v['level'] = $level + 1;
				$v['catName'] = $v['catName'];
				$v['child'] = self::getChildList($cate,$v['catId'],$level + 1);
				array_push($arr,$v);
				// $arr = array_merge($arr,self::getChildList($cate,$v['catId'],$level + 1));
			}
		}
		return $arr;
	 }

	/**
	 * [toggleIsShow 切换状态]
	 * @return [json ] [description]
	 */
	public function toggleIsShow(){
		$id = input('id');
		$status = input('status');
		$data = ['isShow'=>$status];
		$res = $this->db->name('article_cats')->where('catId='.$id)->update($data);
		if($res){
			$result = ['status'=>1,'msg'=>'切换成功','id'=>$id,'state'=>$status];
		}else{
			$result = ['status'=>0,'msg'=>'切换失败'];
		}
		return json($result);
	}

	/**
	 * [editCatName 修改分类名称]
	 * @return [json ] [description]
	 */
	public function editCatName(){
		$id = input('id');
		$catName = input('catName');
		$data = ['catName'=>$catName];
		$res = $this->db->name('article_cats')->where('catId='.$id)->update($data);
		if($res){
			$result = ['status'=>1,'msg'=>'修改成功'];
		}else{
			$result = ['status'=>0,'msg'=>'修改失败'];
		}
		return json($result);
	}

}