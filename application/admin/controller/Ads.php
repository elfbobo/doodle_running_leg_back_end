<?php
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 广告管理控制器
*/
class Ads extends Common
{
	private $db = null;
	private $config = '';
	private $default_config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $userField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->default_config = config('database');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	/**
	 * [index 广告列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = $this->db->name('ads');
		$map = array();
		if(input('adPositionId','-1')!='-1') $map['adPositionId']=input('adPositionId');
		if(input('adName')!='') $map['adName']=['like',"%".input('adName')."%"];
		$listobj = $m
		->where($map)
		->order('adId asc,adSort desc')
		->paginate(10);
		$page = $listobj->render();
		$listarr = $listobj->toArray();
		$list = $listarr['data'];
		foreach ($list as $key => $value) {
			$area1 = $this->db->name('areas')->where('areaId='.$value['areaId1'])->find();
			$area2 = $this->db->name('areas')->where('areaId='.$value['areaId2'])->find();
			$list[$key]['province'] = $area1['areaName'];
			$list[$key]['city'] = $area2['areaName'];
		}
		$this->assign('adName',input('adName/s',''));
		$this->assign('adPositionId',input('adPositionId','-1'));
		$this->assign('page',$page);
		$this->assign('list',$list);
		return $this->fetch('list');
	}

	/**
	 * 跳到新增/编辑页面
	 */
	public function toEdit(){
		//获取地区信息
		$area = $this->db->name('areas');
		//获取商品分类
		$goodsCats = $this->db->name('goods_cats');
		$m = $this->db->name('ads');
		$province = $area->where('areaFlag=1 and isShow = 1 and parentId=0')->select();
		$this->assign('province',$province);
		$id = input('id',0);
    	if($id>0){
    		$item = $m->where('adId='.$id)->find();
    		if(!empty($item)){
				$city = $area->where('areaFlag=1 and isShow = 1 and parentId='.$item['areaId1'])->select();
			}
    		$this->assign('item',$item);
    		$this->assign('city',$city);
    		return $this->fetch('edit');
    	}else{
    		$category = $goodsCats->where('catFlag=1 and parentId=0')->select();
    		$this->assign('category',$category);
			return $this->fetch('add');
    	}
	}

	/**
	 * [add 新增广告]
	 */
	public function add(){
		$m = $this->db->name('ads');
		$data["adPositionId"] = (int)input("adPositionId");
		$data["adStartDate"] = input("adStartDate");
		$data["adEndDate"] = input("adEndDate");
		$data["adSort"] = input("adSort",0);
		$data["adName"] = input("adName");
	    $data["adURL"] = input("adURL");
	    $data["adURLApp"] = input("adURLApp");
		$data["areaId1"] = input("areaId1");
		$data["areaId2"] = input("areaId2");
	    $upload_data = $this->user_upload();
		$data = array_merge($data,$upload_data);
		// dump($data);exit;
		if($m->insert($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('ads/index'),'msg' => '添加成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('ads/index'),'msg' => '添加失败');
		}
		return json($result);
	}

	/**
	 * [edit 修改广告]
	 * @return [json] [description]
	 */
	public function edit(){
		$m = $this->db->name('ads');
		$data["adPositionId"] = (int)input("adPositionId");
		$data["adStartDate"] = input("adStartDate");
		$data["adEndDate"] = input("adEndDate");
		$data["adSort"] = input("adSort",0);
		$data["adName"] = input("adName");
	    $data["adURL"] = input("adURL");
	    $data["adURLApp"] = input("adURLApp");
		$data["areaId1"] = input("areaId1");
		$data["areaId2"] = input("areaId2");
	    $upload_data = $this->user_upload();
		$data = array_merge($data,$upload_data);
		$id = input('id');
		// dump($data);exit;
		$res = $m->where(['adId'=>$id])->update($data);
		if($res){
			$result = array('code' => 1,'reload' => 1,'url' => url('ads/index'),'msg' => '修改成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('ads/index'),'msg' => '修改失败');
		}
		return json($result);
	}

	/**
	 * [del 删除广告]
	 * @return [json] [description]
	 */
	public function del(){
		$m = $this->db->name('ads');
		$id = input('id/d');
		if($m->where(['adId'=>$id])->delete()){
			$result = array('code' => 1,'reload' => 1,'url' => url('ads/index'),'msg' => '添加成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('ads/index'),'msg' => '添加失败');
		}
		return json($result);
	}

	/**
	 * [getCity 获取城市列表]
	 * @return [json] [ajax请求]
	 */
	public function getCity(){
		if (!request()->isAjax()) $this->error('操作失败');
		$m = $this->db->name('areas');
		$data = $m->where(array('parentId' => input('pid'),'areaType'=>1,'isShow'=>1))->select();
		$html = '';
		// 1省，2市，3区
		foreach ($data as $key => $value){
			$html .= '<option value="'.$value['areaId'].'">'.$value['areaName'].'</option>';
		}
		$result = ['status'=>1,'html'=>$html];
		return json($result);
	}

	/**
	 * [user_upload 上传图片]
	 * @return [array] [description]
	 */
	private function user_upload(){
		$adFile = '';
		$adFile = request()->file('adFile');
		// 移动到框架应用根目录/public/uploads/ 目录下
		if ($adFile){
			$info = $adFile->move(ROOT_PATH . DS . config('uplaod') .DS.'face');
			if ($info){
				$adFile = config('uplaod').'/face/'.str_replace('\\', '/', $info->getSaveName());
			}else{
				$this->ajaxReturn(array('code' => 0,'msg' => '上传失败'));
			}
		}
		$data['adFile'] = $adFile;
		return $data;
	}
}