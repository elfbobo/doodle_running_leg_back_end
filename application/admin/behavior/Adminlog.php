<?php
namespace app\admin\behavior;

use think\Request;
use think\Db;

class Adminlog {
	
	public function appInit(&$params){
		//db('admin');
	}
	
	public function run(&$params){
		$request = Request::instance();
		//$request->module($request->dispatch()['module'][0]);
		//$request->controller($request->dispatch()['module'][1]);
		//$request->action($request->dispatch()['module'][2]);
		$module_name = $request->dispatch()['module'][0];
		$controller_name = $request->dispatch()['module'][1];
		$action_name = $request->dispatch()['module'][2];
		
		//不记录的模块
		$not_log_module=config('web_log.not_log_module')?:array();
		
		//不记录的控制器 'module/controller'
		$not_log_controller=config('web_log.not_log_controller')?:array();
		
		//不记录的操作方法 'module/controller/action'
		$not_log_action=config('web_log.not_log_action')?:array();
		
		//不记录data的操作方法 'module/controller/action'	如涉及密码传输的地方：1、前、后台登录runlogin 2、前台重置密码runpwd_reset 3、前台runregister runchangepwd 4、后台member_runadd member_runedit 5、后台admin_runadd admin_runedit
		$not_log_data=['admin/Login/runlogin','home/Login/runlogin','home/Login/runpwd_reset','home/Register/runregister','home/Center/runchangepwd','admin/Member/member_runadd','admin/Member/member_runedit','admin/Admin/admin_runadd','admin/Admin/admin_runedit'];
		$not_log_data=array_merge($not_log_data,config('web_log.not_log_data')?:array());
		
		//不记录的请求类型
		$not_log_request_method=config('web_log.not_log_request_method')?:array();

		if (in_array($module_name, $not_log_module) || in_array($module_name.'/'.$controller_name, $not_log_controller) || in_array($module_name.'/'.$controller_name.'/'.$action_name, $not_log_action) || in_array($request->method(), $not_log_request_method)) {
			return true;
		}
		//只记录存在的操作方法
		if(!has_action($module_name,$controller_name,$request->method())){
			//return true; //全部记录
		}
		try {
			if(in_array($module_name.'/'.$controller_name.'/'.$action_name, $not_log_data)){
				$requestData='保密数据';
			}else{
				$requestData = $request->param();
				foreach ($requestData as &$v) {
					if (is_string($v)) {
						$v = mb_substr($v, 0, 200);
					}
				}
			}
			$session = session('backend');
			$data = [
				'uid'       => $session ? _encrypt($session['id'],'DECODE') : 0,
				'ip'        => $request->ip(),
				'location'  => implode(' ', \Ip::find($request->ip())),
				'os'        => getOs(),
				'browser'   => getBroswer(),
				'url'       => $request->url(),
				'module'    => $module_name ? $module_name : '',
				'controller'=> $controller_name ? $controller_name : '',
				'action'    => $action_name ? $action_name : '',
				'method'    => $request->isAjax()?'Ajax':($request->isPjax()?'Pjax':$request->method()),
				'data'      => serialize($requestData),
				'otime'     => time(),
			];
			Db::name('web_log')->insert($data);
		} catch (Exception $e) {
		}
	}
	
	public function appEnd(&$params){
		//echo 1234;
	}
	
}