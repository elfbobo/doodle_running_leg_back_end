{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					基本配置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">基本设置</a>
				</li>
				<li>
					<a data-toggle="tab" href="#messages">联系方式</a>
				</li>
				</ul>
				<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edit_setting_run');?>">
				<div class="tab-content">
				
					<div id="home" class="tab-pane fade in active">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 网站名称：  </label>
						<div class="col-sm-10">
							<input type="text" name="site_name" id="site_name" placeholder="输入网站名称" value="{$config.site_name}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 网址：  </label>
						<div class="col-sm-10">
							<input type="text" name="url" id="url" placeholder="输入网址" value="{$config.url}" class="col-xs-10 col-sm-7" required/>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 备案信息：  </label>
						<div class="col-sm-10">
							<input type="text" name="ipc" id="ipc" placeholder="输入备案信息" value="{$config.ipc}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 版权信息：  </label>
						<div class="col-sm-10">
							<input type="text" name="copyright" id="copyright" placeholder="输入版权信息" value="{$config.copyright}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 关闭网站： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="open_site" id="open_site" value="1" {if condition="$config['open_site']"}checked="checked"{/if} class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 停止APP数据： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="open_app" id="open_app" value="1" {if condition="$config['open_app']"}checked="checked"{/if} class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					</div>

					<div id="messages" class="tab-pane fade">
						
								<div class="form-group">
									<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 公司名称 </label>
									<div class="col-sm-9">
										<input name="company_name" id="company_name" placeholder="输入公司名称" class="col-xs-10 col-sm-7" value="{$config.company_name}" type="text">
								<span class="help-inline col-xs-12 col-sm-5">
									<span class="middle" id="resone"></span>
								</span>
									</div>
								</div>
								<div class="space-4"></div>

								<div class="form-group">
									<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 公司地址 </label>
									<div class="col-sm-9">
										<input name="company_address" id="company_address" placeholder="输入公司地址" value="{$config.company_address}" class="col-xs-10 col-sm-7" type="text">
								<span class="help-inline col-xs-12 col-sm-5">
									<span class="middle" id="restwo"></span>
								</span>
									</div>
								</div>
								<div class="space-4"></div>
														
								<div class="form-group">
									<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 联系电话 </label>
									<div class="col-sm-9">
										<input name="site_tel" id="site_tel" placeholder="输入联系电话" value="{$config.site_tel}" class="col-xs-10 col-sm-7" type="text">
								<span class="help-inline col-xs-12 col-sm-5">
									<span class="middle" id="restwo"></span>
								</span>
									</div>
								</div>
								<div class="space-4"></div>

								<div class="form-group">
									<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 站长邮箱 </label>
									<div class="col-sm-9">
										<input name="site_admin_email" id="site_admin_email" placeholder="输入站长邮箱" value="{$config.site_admin_email}" class="col-xs-10 col-sm-7" type="email">
								<span class="help-inline col-xs-12 col-sm-5">
									<span class="middle" id="restwo"></span>
								</span>
									</div>
								</div>
								<div class="space-4"></div>

								<div class="form-group">
									<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 站长QQ </label>
									<div class="col-sm-9">
										<input name="site_qq" id="site_qq" value="{$config.site_qq}" placeholder="输入站长QQ" class="col-xs-10 col-sm-7" type="number">
								<span class="help-inline col-xs-12 col-sm-5">
									<span class="middle" id="restwo"></span>
								</span>
									</div>
								</div>
								<div class="space-4"></div>
				
					</div>
					
					
										<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					
					
				</div>
				</form>
			</div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}