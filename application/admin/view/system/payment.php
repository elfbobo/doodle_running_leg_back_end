{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					支付接口配置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">支付宝配置</a>
				</li>
				<li>
					<a data-toggle="tab" href="#wxpay">微信支付配置</a>
				</li>
				</ul>
				<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edit_payment_run');?>">
				<div class="tab-content">
				
					<div id="home" class="tab-pane fade in active">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 短信账户：  </label>
						<div class="col-sm-10">
							<input type="text" name="accountsid" id="accountsid" placeholder="输入短信账户" value="" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>							
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					
					
				</div>
				</form>
			</div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	
});
</script>
{/block}