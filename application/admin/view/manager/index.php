{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('Manager/add_admin')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增管理员
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>ID</th>
			<th>用户名</th>
			<th>管理组</th>
			<th>邮箱</th>
			<th>手机号码</th>
			<th class="hidden-480">真实姓名</th>
			<th class="hidden-480">IP</th>
			<th class="hidden-480">新增时间</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		{foreach name="lists" item="v"}
		<tr id="tr<?php echo $v['id'];?>">
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$v.id}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
<td>
				{$v.id}
			</td>
			<td>
				{$v.username}
			</td><td>
				{if condition="$v['id'] eq 2"}
				系统超级管理员
				{else/}
				{$v.groupname}
				{/if}
			</td>
						<td>
				{$v.email}
			</td>
						<td>
				{$v.mobile}
			</td>	<td class="hidden-480">
				{$v.truename}
			</td>
			<td class="hidden-480">
				{$v.last_ip}
			</td>
			
			<td class="hidden-480">
				{$v.time|date='Y-m-d H:i:s',###}
			</td>
			<td>
				{if condition="$v['id'] neq 2"}
				<?php if ($v['status']){?>
				<button class="btn btn-xs btn-success ajaxStatus" action="<?php echo url('Manager/admin_status');?>" data-id="<?php echo $v['id'];?>">开启</button>
				<?php }else{ ?>
				<button class="btn btn-xs btn-warning ajaxStatus" action="<?php echo url('Manager/admin_status');?>" data-id="<?php echo $v['id'];?>">禁用</button>
				<?php }?>
				{/if}
			</td>

			<td>
			
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('Manager/edit_admin',array('id' => $v['id']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
					{if condition="$v['id'] neq 2"}
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="<?php echo $v['id'];?>" action="<?php echo url('Manager/deleteadmin',array('id' => $v['id']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>
					{/if}
				</div>

				<div class="hidden-md hidden-lg">
					<div class="inline position-relative">
						<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
						</button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							
							<li>
								<a href="#" class="tooltip-success" data-rel="tooltip" title="" data-original-title="Edit">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
									</span>
								</a>
							</li>
							{if condition="$v['id'] neq 2"}
							<li>
								<a href="#" class="tooltip-error" data-rel="tooltip" title="" data-original-title="Delete">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>
									</span>
								</a>
							</li>
							{/if}
						</ul>
					</div>
				</div>
				
			</td>
		</tr>
{/foreach}
		
	</tbody>
</table>
<div class="pager">
<?php echo $page;?>
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

});
</script>
{/block}
