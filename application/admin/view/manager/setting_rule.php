{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div class="row maintop">
<div class="col-xs-12 col-sm-12" style="margin-top:10px;margin-bottom:5px;">
<button class="btn btn-info" id="allno" style="margin-bottom: 10px;">全选/反选</button>
		<form id="bindRole" name="rule_bind_runedit" class="form-horizontal ajaxForm" method="post" action="{:url('rule_bind_runedit')}" role="form">
			<input type="hidden" value="{$data.id}" name="group_id" />
			{foreach name="lists" item="value"}
			<dl class="rule">
				<dt><label for="parent_{$value['id']}"><input type="checkbox" id="parent_{$value['id']}" {if condition="in_array($value['id'],$data['rule_pids'])"}checked="checked"{/if} name="parent[{$value['id']}]" value="{$value['id']}"/>{$value['title']}</label></dt>
				{if condition="isset($value['child'])"}
				<dd>
					{foreach name="value['child']" item="vv"}
						<label for="rule_{$vv['id']}"><input type="checkbox" id="rule_{$vv['id']}" {if condition="in_array($vv['id'],$data['rules'])"}checked="checked"{/if} name="rule[{$vv['id']}]" value="{$vv['id']}"/>{$vv['title']}</label>
						{foreach name="vv['child']" item="v3"}
							<label for="rule_{$v3['id']}"><input type="checkbox" id="rule_{$v3['id']}" {if condition="in_array($v3['id'],$data['rules'])"}checked="checked"{/if} name="rule[{$v3['id']}]" value="{$v3['id']}"/>{$v3['title']}</label>
						{/foreach}
					{/foreach}
				</dd>
				{/if}
			</dl>
			{/foreach}
			<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit">
						<i class="icon-ok bigger-110"></i>提交绑定
					</button>
			</div>
		</form>
</div>
</div>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<style type="text/css">
.rule{padding:0 5px 5px 0px;margin-bottom:5px;}
.rule dt{padding:5px 10px;border:1px solid #ccc;margin-top:0px;}
.rule dt{border-top-right-radius:4px;border-top-left-radius:4px;}
.rule dt label{font-weight:bold;}
.rule dd label{font-size:12px;margin-left:5px;}
.rule dd{border:1px solid #ccc;padding:10px;margin-bottom:0px;border-top:none;border-bottom-right-radius:4px;border-bottom-left-radius:4px;}
.rule dt input,.rule dd input{vertical-align:sub;margin-right:5px;}
</style>
<script type="text/javascript">
$(function(){
	var flag = true;
	$(document).on('click','#allno',function(){
		if(flag){
			flag = false;
			$('dl').find('input:checkbox').each(function(){
				this.checked = true;
			});
		}else{
			$('dl').find('input:checkbox').each(function(){
				this.checked = false;
			});
			flag = true;
		}
	});
	$(document).on('click', 'dt input:checkbox' , function(){
		var _that = this.checked;
		$(this).parents('dt').next('dd').find('input:checkbox').each(function(){
			this.checked = _that;
		});
	});
	$(document).on('click', 'dd input:checkbox' , function(){
		var count = 0;
		var _this = $(this);
		$(this).parents('dl').find('dt input:checkbox').each(function(){
			_this.parents('dl').find('dd input:checkbox').each(function(){
				if(this.checked){
					count++;
				}
			});
			var _that = count > 0 ? true : false;
			this.checked = _that
		});
	});
});
</script>
{/block}