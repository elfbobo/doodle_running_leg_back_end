{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">订单号</span>
		</label>
		<input class="input" name="id" value="{$id}" placeholder="请输入订单号" type="text">
		<label class="inline">
			<span class="lbl">订单状态</span>
		</label>
		<select name="pay_status" class="form-control" id="form-field-select-1" style="display: none;">
		<option value="0" {if condition="$pay_status=='0'"}selected{/if}>全部</option>
		<option value="2" {if condition="$pay_status=='2'"}selected{/if}>退款</option>
		<option value="3" {if condition="$pay_status=='3'"}selected{/if}>退款成功</option>
		<option value="4" {if condition="$pay_status=='4'"}selected{/if}>拒绝退款</option>
		</select>
		<label class="inline">
			<span class="lbl">用户账号</span>
		</label>
		<input class="input" name="username" value="{$username}" placeholder="请输入账号" type="text">
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>订单号</th>
			<th>用户名</th>
			<th>订单金额/支付方式</th>
			<th>送货地址</th>
			<th>目的地址</th>
			<th>订单状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($list))foreach ($list as $key => $value) { ?>
		<tr>
			<td>{$value.id}</td>
			<td>{$value.username}</td>
			<th>{$value.money+$value.extra_money}<br/>{$value.paytype}</th>
			<td>{$value.content.go_address}{$value.content.go_number}</td>
			<td>{$value.content.get_address}{$value.content.get_number}</td>
			<td>{$value.paystatus}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
					<button class="btn btn-danger btn-xs orderDetail" data-id="{$value.id}"><i class="ace-icon fa fa-eye bigger-110"></i>查看</button>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>

<div class="pager">
{$page}
</div>


</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail',array('op'=>'refund'));?>?id='+orderId
		    });
		}
	});
});
</script>
{/block}
