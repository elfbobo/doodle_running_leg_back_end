{extend name="public/base" /}
{block name="main"}
<style>.list-unstyled li{padding:5px 0px;}#sidebar2{display:none !important;}.main-container,.page-content{margin-top:0px !important;padding-top:5px !important;}</style>
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div id="user-profile-1" class="user-profile row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="space-6"></div>

<h5 style="padding:10px;">订单号：{$orderInfo['id']}<span style="padding-left: 140px;">状态：{$orderInfo['orderstatus']}</span></h5>
<div class="hr hr2"></div>

<h5 style="padding:10px;">订单日志</h5>
<div class="list-unstyled" style="padding:10px;">
	<div style="display: inline-block;">
		<div style="display: inline-block;">操作时间：{$orderInfo['time']|date='Y-m-d H:i:s',###}</div>
		<div style="display: inline-block;padding-left: 140px;">操作信息：{$orderInfo['paystatus']}</div>
		<div style="display: inline-block;padding-left: 140px;">操作人：{$orderInfo['username']}</div>
	</div>
</div>
<div class="hr hr2"></div>

<h5 style="padding:10px;">订单信息</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		支付方式：{$orderInfo['content']['paytype']}
	</li>
	{if condition="$orderInfo['order_status']<=0"}
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		期望到货时间：{$orderInfo['content']['send_time']}
	</li>
	{else /}
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		要求到货时间：<?php if(!empty($orderInfo['require'])){
			echo date('Y-m-d H:i:s',$orderInfo['require_time']);
			}else{
				echo '无要求';
			}?>
	</li>
	{/if}
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		买家留言：{$orderInfo['content']['remark']}
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">取货地址</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		<font style="color:#f60;">取</font>：{$orderInfo['content']['go_address']}{$orderInfo['content']['go_number']} &nbsp;&nbsp;(取货地址)
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系人：{$orderInfo['content']['go_username']}
	</li>		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系电话：{$orderInfo['content']['go_mobile']}
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">目的地址</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		<font style="color:#f60;">送</font>：{$orderInfo['content']['get_address']}{$orderInfo['content']['get_number']} &nbsp;&nbsp;(目的地址)
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系人：{$orderInfo['content']['get_username']}
	</li>		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系电话：{$orderInfo['content']['get_mobile']}
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">配送信息</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		配送订单号：{$orderInfo['id']}
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系号码：<strong style="font-size: 18px;color:#f60;">{$orderInfo['deliveryMobile']}</strong> 
		<!-- 联系号码：<strong style="font-size: 18px;color:#f60;">{$orderInfo['content']['get_mobile']}</strong>  -->
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		配送员：{$orderInfo['deliveryUserName']}
		<!-- 配送员：{$orderInfo['content']['get_username']} -->
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		配送费用：<strong style="font-size: 18px;color:#f60;">{$orderInfo['money']}</strong> 元
	</li>
</ul>
<div class="hr hr2"></div>




</div>
</div>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">

</script>
{/block}
