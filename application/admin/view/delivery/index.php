{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('Delivery/add_user')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增配送员
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>ID</th>
			<th>手机号</th>
			<th>真实姓名</th>
			<th>余额</th>
			<th>待结算余额</th>
			<th class="hidden-480">最后登录时间</th>
			<th class="hidden-480">新增时间</th>
			<th>状态</th>
			<th>审核状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		{foreach name="lists" item="v"}
		<tr>
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$v.id}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td>{$v.id}</td>
			<td>{$v.mobile}</td>
			<td>{$v.truename}</td>
			<td>{$v.money}</td>
			<td>{$v.settlement_money}</td>
			<td class="hidden-480"><?php if ($v['last_time']):?>
			{$v.last_time|date='Y-m-d H:i:s',###}
			<?php else: ?>
			未登录
			<?php endif;?>
			</td>
			<td class="hidden-480">{$v.time|date='Y-m-d H:i:s',###}</td>
			<td>
			<?php if ($v['status']){?>
				<button class="btn btn-xs btn-success ajaxStatus" action="<?php echo url('Delivery/user_status');?>" data-id="<?php echo $v['id'];?>">开启</button>
				<?php }else{ ?>
				<button class="btn btn-xs btn-warning ajaxStatus" action="<?php echo url('Delivery/user_status');?>" data-id="<?php echo $v['id'];?>">禁用</button>
				<?php }?>
			</td>
						<td>
			<?php if ($v['check_status']){?>
				<button class="btn btn-xs btn-success ajaxStatus" action="<?php echo url('Delivery/check_status');?>" data-id="<?php echo $v['id'];?>">通过</button>
				<?php }else{ ?>
				<button class="btn btn-xs btn-warning ajaxStatus" action="<?php echo url('Delivery/check_status');?>" data-id="<?php echo $v['id'];?>">未通过</button>
				<?php }?>
			</td>
			<td>
			
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('Delivery/edit_user',array('id' => $v['id']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
	
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="" action="<?php echo url('Delivery/deleteuser',array('id' => $v['id']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>

				</div>

				<div class="hidden-md hidden-lg">
					<div class="inline position-relative">
						<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
						</button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							
							<li>
								<a href="#" class="tooltip-success" data-rel="tooltip" title="" data-original-title="Edit">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
									</span>
								</a>
							</li>
					
							<li>
								<a href="#" class="tooltip-error" data-rel="tooltip" title="" data-original-title="Delete">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>
									</span>
								</a>
							</li>
			
						</ul>
					</div>
				</div>
				
			</td>
		</tr>
{/foreach}
		
	</tbody>
</table>
<div class="pager">
	{$page}
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	
});
</script>
{/block}
