{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>序号</th>
			<th>订单号</th>
			<th>类型</th>
			<th>金额</th>
			<th>配送员</th>
		</tr>
	</thead>

	<tbody>
		<if condition="isset($lists)">
		{foreach name="lists" item="v"}
		<tr id="tr">
			<td>{$v.id}</td>
			<td>{$v.orderid}</td>
			<td class="hidden-480">
			{if condition="$v['type'] == 2"}
				提现
			{else/}
				订单配送完成
			{/if}
			</td>
			<td>{$v.money}</td>
			<td>{$v.mobile}({$v.truename})</td>
		</tr>
		{/foreach}
	</if>
	</tbody>
</table>
<?php if (isset($page)):?>
<div class="pager">
<?php echo $page;?>
</div>
<?php endif;?>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}

<script type="text/javascript">
$(function(){
	
});
</script>
{/block}
