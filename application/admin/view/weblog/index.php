{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<p>
<button id="delete_all" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-trash-o bigger-110"></i>批量删除日志</button>
<button id="clear_all" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-trash-o bigger-110"></i>清空日志</button>
</p>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>UID</th>
			<th>IP地址</th>
			<th>模块/控制器/方法</th>
			<th>操作地址</th>
			<th>请求方法</th>
			<th>请求时间</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$value.id}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td>{$value.uid}</td>
			<td title="{$value.location}">{$value.ip}</td>
			<td>{$value.module}/{$value.controller}/{$value.action}</td>
			<td title="{$value.browser}">{$value.url}</td>
			<td>{$value.method}</td>
			<td>{$value.otime|date='Y-m-d H:i:s',###}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
                  	<button class="btn btn-xs btn-danger ajaxDelete" data-id="" action="<?php echo url('delete',array('id' => $value['id']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#clear_all').click(function(){
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('clear_all');?>',data:{},success:function(data){
				var data = $.parseJSON(data);
				if(data.code){
					layer.msg(data.msg, function(){});window.location.reload();} 
			}});
		}
	});
	$('#delete_all').click(function(){
		var tables = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				tables += $(this).val()+'|'
			}
		});
		if(tables == '') {
			layer.msg('至少选择一行', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('delete_all');?>',data:{tables:tables},success:function(data){
				var data = $.parseJSON(data);
				if(data.code){
					layer.msg(data.msg, function(){});window.location.reload();} 
			}});
		}
	});
	
});
</script>
{/block}
