{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->

<div class="page-header">
	<h4>系统目录下共有{$countFile}个zip文件，共计{$totalSize}</h4>
</div>

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>文件名</th>
			<th>打包时间</th>
			<th>占用大小</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr id="tr<?php echo strtotime($value['time']);?>">
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$value.file}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td><a href="<?php echo url('download',array('type' => 'zip','file' => $value['file']));?>">{$value.file}</a></td>
			<td>{$value.time}</td>
			<td>{$value.size}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
                   <button class="btn btn-xs btn-danger import" action="<?php echo url('unzip',array('file' => $value['file']));?>"><i class="ace-icon fa fa-undo bigger-110"></i>解压</button>
				   <button class="btn btn-xs btn-danger ajaxDelete" data-id="<?php echo strtotime($value['time']);?>" action="<?php echo url('removezipfile',array('file' => $value['file']));?>"><i class="ace-icon fa fa-trash-o bigger-110"></i>删除</button>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<p>
<button id="deleteAll" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-trash-o bigger-110"></i>删除文件</button>
</p>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

	$('.import').click(function(){
		if(confirm('确认执行此操作？')){
			var _url = $(this).attr('action');
			$.ajax({type:'POST',url:_url,data:{},success:function(data){
				var data = $.parseJSON(data);
				if(data.code){
					var index = layer.open({
						  type: 1,
						  shade: false,
						  title: false, //不显示标题
						  content: data.msg, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
						  cancel: function(){
						    
						  }
						});
					layer.style(index, {
						padding: '15px'
						});  
				}
			}});
		}
	});

	$('#zip').click(function(){
		var file = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				file += $(this).val()+'|'
			}
		});
		if(file == '') {
			layer.msg('至少选择一个文件', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('ziphandler');?>',data:{sqlFiles:file},success:function(data){
				if(data.code){
					var index = layer.open({
						  type: 1,
						  shade: false,
						  title: false, //不显示标题
						  content: data.msg, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
						  cancel: function(){
						    
						  }
						});
					layer.style(index, {
						  padding: '15px'
						});       
				}
			}});
		}
	});
	
	$('#deleteAll').click(function(){
		var tables = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				tables += $(this).val()+'|'
			}
		});
		if(tables == '') {
			layer.msg('至少选择一个文件', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('removezipfile');?>',data:{files:tables},success:function(data){
				var data = $.parseJSON(data);
				if(data.code) window.location.reload();
			}});
		}
	});
	
});
</script>
{/block}
