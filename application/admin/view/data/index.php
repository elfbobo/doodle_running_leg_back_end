{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->

<div class="page-header">
	<h4>数据库中共有{$countTable}张表，共计{$totalSize}</h4>
</div>

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>表名</th>
			<th>引擎类型</th>
			<th>字符集</th>
			<th>记录行数</th>
			<th>占用大小</th>
			<th>创建时间</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$value.Name}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td>{$value.Name}</td>
			<td>{$value.Engine}</td>
			<td>{$value.Collation}</td>
			<td>{$value.Rows}</td>
			<td>{$value.size}</td>
			<td>{$value.Create_time}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
                  <a class="green aclick" href="<?php echo url('optimize',array('table' => $value['Name']));?>" title="优化表">
                      <i class="ace-icon fa fa-check-circle bigger-130"></i>
                   </a>
                 <a class="info aclick" href="<?php echo url('repair',array('table' => $value['Name']));?>" title="修复表">
                    <i class="ace-icon fa fa-check-square-o bigger-130"></i>
                </a>
                   <a class="info backup" href="<?php echo url('exportsql',array('table' => $value['Name']));?>" title="备份表">
                     <i class="ace-icon fa fa-download bigger-130"></i>
                </a>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<p>
<button id="optimize" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-check-circle bigger-110"></i>优化数据表</button>
<button id="repair" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-check-square-o bigger-110"></i>修复数据表</button>
<button id="backup" class="btn btn-sm btn-danger"><i class="ace-icon fa fa-database bigger-110"></i>开始备份</button>
</p>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

	$('#backup').click(function(){
		var tables = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				tables += $(this).val()+'|'
			}
		});
		if(tables == '') {
			layer.msg('至少选择一张表', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			var index = layer.load(1, {
				  shade: [0.2,'#333'] //0.1透明度的白色背景
				});
			$.ajax({type:'POST',url:'<?php echo url('backuphandle');?>',data:{tables:tables},success:function(data){
				var data = $.parseJSON(data);
				layer.msg(data.msg, function(){});
				if(data.code){
					window.location.href=data.url;
				}
			}});
		}
	});
	
	$('.action-buttons a.aclick').click(function(){
		if(confirm('确认执行此操作？')){
			var _url = $(this).attr('href');
			$.ajax({type:'GET',url:_url,data:{},success:function(data){
				var data = $.parseJSON(data);
				if(data.code) {layer.msg(data.msg, function(){});setTimeout(function(){window.location.reload();},1000);}
			}});
		}
		return false;
	});
	
	$('#repair').click(function(){
		var tables = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				tables += $(this).val()+'|'
			}
		});
		if(tables == '') {
			layer.msg('至少选择一张表', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('repair');?>',data:{tables:tables},success:function(data){
				var data = $.parseJSON(data);
				if(data.code) window.location.reload();
			}});
		}
	});
	
	$('#optimize').click(function(){
		var tables = '';
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				tables += $(this).val()+'|'
			}
		});
		if(tables == '') {
			layer.msg('至少选择一张表', function(){});
			return;
		}
		if(confirm('确认执行此操作？')){
			$.ajax({type:'POST',url:'<?php echo url('optimize');?>',data:{tables:tables},success:function(data){
				var data = $.parseJSON(data);
				if(data.code){
					layer.msg(data.msg, function(){});window.location.reload();} 
			}});
		}
	});
	
});
</script>
{/block}
