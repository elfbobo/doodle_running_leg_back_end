{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">店铺编号</span>
		</label>
		<input class="input" name="shopSn" value="{$shopSn}" placeholder="请输入店铺编号" type="text">
		<label class="inline">
			<span class="lbl">店铺名称</span>
		</label>
		<input class="input" name="shopName" value="{$shopName}" placeholder="请输入店铺名称" type="text">
		<label class="inline">
			<span class="lbl">店铺电话</span>	
		</label>
		<input class="input" name="shopTel" value="{$shopTel}" placeholder="请输入店铺电话" type="text">
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>店铺ID</th>
			<th>店铺编号</th>
			<th>店铺名称</th>
			<th>店铺电话</th>
			<th>店铺地址</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { 
			$ids = array($value['areaId1'],$value['areaId2'],$value['areaId3']);
			$area = $db->name('areas')->where(array('areaId' => array('IN',$ids)))->field('areaName')->select();
		?>
		<tr>
			<td>{$value.shopId}</td>
			<td>{$value.shopSn}</td>
			<td>{$value.shopName}</td>
			<td>{$value.shopTel}</td>
			<td>
			<?php foreach ($area as $a => $b){?>
				<?php echo $b['areaName'];?>-
			<?php }?>{$value.shopAddress}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
				<button class="btn btn-danger btn-xs settingArea" data-id="{$value.shopId}"><i class="ace-icon fa fa-cogs bigger-110"></i>设置配送区域</button>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<?php if (isset($page)):?>

{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

<?php endif;?>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.settingArea').click(function(){
		var shopid = $(this).attr('data-id');
		if(shopid){
		    var index = layer.open({
		        type: 2,
		        title: '设置配送区域',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['800px', '500px'],
		        content: '<?php echo url('setting');?>?shopid='+shopid
		    });
		}
	});
});
</script>
{/block}
