{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					退单设置
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm1" name="refundRun" method="post" action="{:url('admin/setting/refundRun')}">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 扣除用户跑腿费的百分比：  </label>
						<div class="col-sm-9">
							<input type="text" name="value" value="{$config['value']}" class="col-xs-10 col-sm-4" id="value" placeholder="例如：10%" />
						<span class="middle">（配送员接单后未取餐用户退单的扣除比例）</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<a class="btn" href="javascript:window.history.go(-1);">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								返回
							</a>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
//ajax提交表单
	$('.ajaxForm1').submit(function(){
			var _this = $(this);
			var url = _this.attr('action');
			var index = '';
			var data = $("#value").val();
			// 正则验证
			var pattern = /^(100|[1-9]?\d(\.\d\d?)?)%$/;
			if(pattern.test(data)){
				$(this).ajaxSubmit({
					type : 'POST',
					url : url,
					beforeSend : function(e){
						index = layer.load(1, {
						  	shade: [0.2,'#333']
						});
					},
					success : function(data){
						//返回数据()
						if(typeof data == 'string'){
							var data = $.parseJSON(data);
						}
						if(data.reset == 1){_this.resetForm();}
						if(data.reload == 1 && data.url != undefined){
							window.location.href = data.url;
						}else{
							window.location.reload();
						}
						layer.msg(data.msg, function(){});
						layer.close(index);
					},
				});				
			}else{
				layer.msg('请输入0%-100%的百分数')
				parent.layer.close(index)
				return false;
			}
		return false;
	});
</script>
{/block}