{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					代理提成设置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">代理提成设置</a>
				</li>

				</ul>
				<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('agentRun');?>">
				<div class="tab-content">
	<div class="form-group" style="padding-top: 15px;">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1">店铺代理提成点：  </label>
		<div class="col-sm-10">
			<input type="text" name="agent_shop_bili" placeholder="输入比例" value="<?php if (isset($user_shop['value'])){echo $user_shop['value'];}?>" class="col-xs-2 col-sm-2"/>
			<span class="help-inline col-xs-12 col-sm-5">
			<span class="middle" id="resone">（平台拿商家抽佣的提成点）</span>
			</span>
		</div>
	</div>	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-2">跑腿代理提成点：  </label>
		<div class="col-sm-10">
			<input type="text" name="agent_delivery_bili" placeholder="输入比例" value="<?php if (isset($user_delivery['value'])){echo $user_delivery['value'];}?>" class="col-xs-2 col-sm-2"/>
			<span class="help-inline col-xs-12 col-sm-5">
			<span class="middle" id="resone">（收取用户配送费减去给配送员的配送费后的金额的百分点）</span>
			</span>
		</div>
	</div>
						<div class="clearfix appendHmtl">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					
					
				</div>
				</form>
			</div>

        </section>

				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}

<script type="text/javascript">
$(function(){

	$('body').on('click','.btn-delete',function(){
		$(this).parent().remove();
	});
	
	$('.add-config').click(function(){

		$('.appendHmtl').before($('template').html());

	});
	
});
</script>
{/block}