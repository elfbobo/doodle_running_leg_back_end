{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


					<section class="content">

            <div class='panel wst-panel-full'>
         <div class="col-xs-12 wstmall-login-tips">
             
         </div>        
         <div class='row' style='padding-left:10px;margin-right:10px;'>
             <div class="col-md-9">
               <div class="box-header">
                 <h4 class="text-blue">一周动态</h4>
                 <table class="table table-hover table-striped table-bordered wst-form">
                    <tr>
                       <td width="5%" align='right'>新增会员数：</td>
                       <td width="30%">{$weekInfo.userNew}</td>
                       <!-- <td width="20%" align='right'>新增店铺数/申请数：</td> -->
                       <!-- <td width="30%">{$weekInfo.shopNew}/{$weekInfo.shopApply}</td> -->
                    </tr>
                    <tr>
                       <!-- <td align='right'>新增商品数：</td> -->
                       <!-- <td>{$weekInfo.goodsNew}</td> -->
                       <td align='right'>新增订单数：</td>
                       <td>{$weekInfo.ordersNew}</td>
                    </tr>
                 </table>
               </div>
               <div class="box-header">
                 <h4 class="text-blue">统计信息</h4>
                 <table class="table table-hover table-striped table-bordered wst-form">
                    <tr>
                       <td width="5%" align='right'>会员总数：</td>
                       <td width="30%">{$sumInfo.userSum}</td>
                       <!-- <td width="20%" align='right'>店铺总数/申请总数：</td> -->
                       <!-- <td width="30%">{$sumInfo.shopSum}/{$sumInfo.shopApplySum}</td> -->
                    </tr>
                    <tr>
                       <!-- <td align='right'>商品总数：</td> -->
                       <!-- <td>{$sumInfo.goodsSum}</td> -->
                       <td align='right'>订单总数：</td>
                       <td>{$sumInfo.ordersSum}</td>
                    </tr>
                    <tr>
                       <td width="200" align='right'>订单总金额</td>
                       <td width="300" colspan='3'>{$sumInfo.moneySum}</td>
                    </tr>
                 </table>
               </div>
<!--                <div class="box-header">
                 <h4 class="text-blue">系统信息</h4>
                 <table class="table table-hover table-striped table-bordered wst-form">
                    <tr>
                       <td width="20%" align='right'>软件版本号：</td>
                       <td>{:config('WST_VERSION')}</td>
                       <td align='right'>授权类型：</td>
                       <td><div id='licenseStatus'>无</div></td>
                    </tr>
                    <tr>
                       <td align='right'>问题反馈：</td>
                       <td id='webUrl'><a href="{:config('WST_FEEDBACK')}" target='_blank'>点击反馈</a></td>
                       <td align='right'>授权码：</td>
                       <td>无</td>
                    </tr>
                    <tr>
                       <td align='right'>服务器操作系统：</td>
                       <td>{$Think.PHP_OS}</td>
                       <td align='right'>WEB服务器：</td>
                       <td >{$Think.server.SERVER_SOFTWARE}</td>
                    </tr>
                    <tr>
                       <td align='right'>PHP版本：</td>
                       <td >{$Think.PHP_VERSION}</td>
                       <td align='right'>MYSQL版本：</td>
                       <td ></td>
                    </tr>
                 </table>
               </div> -->
            </div>
            <div class="col-md-3">
               <div class="box-header" style='margin-bottom:30px;'>
                 <h4 class='wstmall-intro'>走进我们</h4>
                 <div><a style='color:#000000' href="{:config('WST_WEB')}" target='_blank'>{:config('WST_WEB')}</a></div>
               </div>
               <div class="box-header" style='margin-bottom:30px;'>
                 <h4 class='wstmall-intro'>我们的理念</h4>
                 <div>我们愿与更多中小企业一起努力，一起成功!</div>
               </div>
               <div class="box-header">
                 <h4 class='wstmall-intro'>商城定制</h4>
                 <div>电话：020-0888888</div>
                 <div style='padding-left:5px;'>QQ：8706659</div>
               </div>
            </div>
          </div>  
      </div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

{/block}
