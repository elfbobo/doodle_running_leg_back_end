{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">文章标题</span>
		</label>
		<input class="input" name="articleTitle" value="{$articleTitle}" placeholder="文章标题" type="text">
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('articles/toEdit')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增
</button>
</a>
</div>
</div>

<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>序号</th>
           <th>标题</th>
           <th>分类</th>
           <th>是否显示</th>
           <th>最后编辑者</th>
           <th>创建时间</th>
           <th>操作</th>
		</tr>
	</thead>

	<tbody>
		{volist name="list" id="vo"}
		<tr>
		   <td>{$i}</td>
           <td>{$vo['articleTitle']}</td>
           <td>{$vo['catName']}</td>
           <td>
           <div class="dropdown">
           	{if condition="$vo['isShow'] eq 0"}
			    <button type="button" class="btn dropdown-toggle" id="btn_{$vo['articleId']}" data-status="{$vo.isShow}" data-toggle="dropdown">隐藏<span class="caret"></span></button>
			{else /}
				<button type="button" class="btn dropdown-toggle" id="btn_{$vo['articleId']}" data-toggle="dropdown">显示<span class="caret"></span></button>
			{/if}
			    <ul class="dropdown-menu" role="menu" aria-labelledby="btn_{$vo['articleId']}">
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(1,{$vo['articleId']});">显示</a></li>
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(0,{$vo['articleId']});">隐藏</a></li>
			    </ul>
			</div>
           </td>
           <td>{$vo['staffName']}</td>
           <td>{$vo['createTime']}</td>
			<td style="width:160px;">		
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('articles/toEdit',array('id' => $vo['articleId']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="{$vo.articleId}" action="<?php echo url('articles/del',array('id' => $vo['articleId']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>
				</div>
			</td>
		</tr>
		{/volist}
		
	</tbody>
</table>
<div class="pager">
{$page}
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
	//状态切换
	function toggleIsShow(status,id){
		var message = status?'显示':'隐藏'
		var status = status
		var id = id
		var url = "{:url('admin/articles/toggleIsShow')}";
		$.post(url,{id:id,status:status},function(e){
			if(e.status==1){
				$("#btn_"+id).attr('data-status',e.state)
				$("#btn_"+id).html(message+'<span class="caret"></span>');
				layer.msg(e.msg);
				location.reload();
			}else{
				layer.msg(e.msg);
				location.reload();
				return false;
			}
		},'json')
	}

	//修改分类名称
	// function editCatName(id,obj){
	// 	var catName = $(obj).val();
	// 	if(catName == ''){
	// 		layer.msg('不能为空');
	// 		return false
	// 	}
	// 	var id = id
	// 	var url = "{:url('admin/articlecats/editCatName')}";
	// 	$.post(url,{id:id,catName:catName},function(e){
	// 		if(e.status==1){
	// 			$(obj).val(catName)
	// 			layer.msg(e.msg);
	// 		}else{
	// 			layer.msg(e.msg);
	// 			return false;
	// 		}
	// 	},'json')
	// }

</script>
{/block}
