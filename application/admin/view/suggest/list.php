{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-2">
<a href="{:url('admin/banks/toEdit')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增
</button>
</a>
</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>序号</th>
			<th>用户名</th>
			<th>联系方式</th>
			<th>用户建议</th>			
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
	{volist name="list" id="vo"}
		<tr id="data_{$vo.id}">
			<td>{$i}</td>
			<td>{$vo.userName}</td>
			<td>{$vo.phone}</td>
			<td><text>{$vo.content}</text></td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
				<button type="button" class="btn btn-default glyphicon glyphicon-trash" onclick="del({$vo['id']});">刪除</button>
            </div>
			</td>
		</tr>
	{/volist}
	</tbody>
</table>
<div class="pager">
{$page}
</div>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
   function del(id){
   		var msg = '您确定要删除该建议信息吗?';
		layer.confirm(msg, {btn: ['取消', '确定'],btn2: function(index, layero){
		   //确定
			$.post("{:url('admin/suggest/del')}",{id:id},function(data){
				if(data.status=='1'){
					layer.msg(data.msg);
					$("#data_"+data.id).remove();
					location.reload();
				}else{
					return false
				}
				},'json');
			}
		}, function(index){
		  //取消
		  parent.layer.close(index);
		});
   }
</script>
{/block}
