{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">广告位置</span>
		</label>
		<select name="adPositionId" class="form-control" id="userStatus">
		<option value="-1" {if condition="$adPositionId=='-1'"}selected{/if}>全部</option>
		<option value="0" {if condition="$adPositionId==0"}selected{/if}>微帮</option>
		<!-- <option value="1" {if condition="$adPositionId==1"}selected{/if}>云购</option> -->
		<option value="2" {if condition="$adPositionId==2"}selected{/if}>积分</option>
		<option value="3" {if condition="$adPositionId==3"}selected{/if}>新店来袭</option>
		</select>
		<label class="inline">
			<span class="lbl">广告标题</span>
		</label>
		<input class="input" name="adName" value="{$adName}" placeholder="请输入广告标题" type="text">
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('ads/toEdit')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
			</th>
			<th>序号</th>
			<th>广告标题</th>
			<th>广告位置</th>
			<th>PC跳转网址</th>
			<th>APP跳转</th>
			<th>广告日期</th>
			<th>所属地区</th>
			<th>图标</th>
			<th>点击数</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		{volist name="list" id="vo"}
		<tr>
			<td class="center">
			</td>
			<td>{$i}</td>
               <td>{$vo['adName']}</td>
               <td>
               <if condition="$vo['adPositionId'] == -1 ">首页主广告
               <elseif condition="$vo['adPositionId'] == -2 "/>品牌汇广告
               <elseif condition="$vo['adPositionId'] == -3 "/>店铺街广告
               <elseif condition="$vo['adPositionId'] == -4 "/>首页副广告
               <elseif condition="$vo['adPositionId'] == -5 "/>首页商城头条
               <elseif condition="$vo['adPositionId'] == 0 "/>微帮
               <elseif condition="$vo['adPositionId'] == 1 "/>云购
               <elseif condition="$vo['adPositionId'] == 2 "/>积分商城
               <elseif condition="$vo['adPositionId'] == 3 "/>新店来袭
              </if>
               </td>
               <td>{$vo['adURL']}</td>
               <td>{$vo['adURLApp']}</td>
               <td>{$vo['adStartDate']}至{$vo['adEndDate']}</td>
               <td>{$vo['province']}{$vo['city']}</td>
               <td><img src='__ROOT__/{$vo['adFile']}' width='60' height='30'></td>
               <td>{$vo['adClickNum']}</td>
			<td style="width:80px;">		
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('ads/toEdit',array('id' => $vo['adId']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
	
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="" action="<?php echo url('ads/del',array('id' => $vo['adId']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>

				</div>	
			</td>
		</tr>
{/volist}
		
	</tbody>
</table>
<div class="pager">
{$page}
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	
});
</script>
{/block}
