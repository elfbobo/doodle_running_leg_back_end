{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div class="row maintop">
<div class="col-xs-12 col-sm-12">
<a href="{:url('index')}">
<button class="btn btn-sm btn-danger">全部</button>
</a>
<a href="{:url('index',array('type' => 1))}">
<button class="btn btn-sm btn-danger">有效</button>
</a>
<a href="{:url('index',array('type' => 2))}">
<button class="btn btn-sm btn-danger">失效</button>
</a>
<a href="{:url('add')}" style="float:right;">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>标题</th><th>减免金额</th>
			<th>失效时间</th><th>发布时间</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		{foreach name="lists" item="v"}
		<tr id="tr<?php echo $v['id'];?>">
					<td>{$v.id}</td>
			<td>
				{$v.title}
			</td><td>
				{$v.money}
			</td><td>
				{$v.endtime|date='Y-m-d H:i:s',###}
			</td><td>
				{$v.time|date='Y-m-d H:i:s',###}
			</td>
			<td>
				<div class="hidden-sm hidden-xs btn-group">
<!-- 					<button class="btn btn-xs btn-danger ajaxDelete" data-id="<?php echo $v['id'];?>" action="<?php echo url('delete',array('id' => $v['id']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button> -->
					<?php if ($v['type'] == 1){?>
						全部用户
					<?php }else{ ?>
						部分用户 <a href="<?php echo url('senduser',array('id' => $v['id']));?>">(查看详情)</a>
					<?php }?>
				</div>
			</td>
		</tr>
{/foreach}
		
	</tbody>
</table>
<div class="pager">
<?php echo $page;?>
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

});
</script>
{/block}
