{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>UID</th>
			<th>标题</th><th>减免金额</th>
			<th>发布时间</th>
		</tr>
	</thead>

	<tbody>
		{foreach name="lists" item="v"}
		<tr id="tr<?php echo $v['id'];?>">
					<td>{$v.id}</td>
					<td>{$v.uid}</td>
			<td>
				{$v.title}
			</td><td>
				{$v.money}
			</td><td>
				{$v.time|date='Y-m-d H:i:s',###}
			</td>
			
		</tr>
{/foreach}
		
	</tbody>
</table>
<div class="pager">
<?php echo $page;?>
</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

});
</script>
{/block}
