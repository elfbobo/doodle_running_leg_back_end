{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="get" action="">
		<label class="inline">
			<span class="lbl">会员账号</span>
		</label>
		<input class="input" name="loginName" value="{$loginName}" placeholder="请输入账号" type="text">
		<label class="inline">
			<span class="lbl">手机号码</span>
		</label>
		<input class="input" name="userPhone" value="{$userPhone}" placeholder="请输入用户手机号码" type="text">
		<label class="inline">
			<span class="lbl">会员类型</span>
		</label>
		<select name="userType" class="form-control" id="userType">
		<option value="-1">全部</option>
		<option value="1" <?php if ($userType == 1) echo 'selected="selected"';?>>店铺会员</option>
		<option value="0" <?php if ($userType == 0) echo 'selected="selected"';?>>普通会员</option>
		</select>
		
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-2">
<a href="{:url('User/toEdit')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增会员
</button>
</a>
</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>账号</th>
			<th>手机号码</th>
			<th>余额</th>
			<th>注册时间</th>
			<th>启用状态</th>
			<th>审核状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
	<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td>{$key+1}</td>
			<td>{$value.loginName}</td>
			<td>{$value.userPhone}</td>
			<td>{$value.userMoney}</td>
			<td>{$value.createTime}</td>
			<td style="cursor:pointer;">
				<div id="data_{$value['userId']}"  style="cursor:pointer;">
	           	{if condition ="$value['userStatus']==0"}
					<span data-status="{$value['userStatus']}" onclick="toggleUserStatus({$value['userId']},this)">停用</span>
	           	{else/}
					<span data-status="{$value['userStatus']}" onclick="toggleUserStatus({$value['userId']},this)">启用</span>
				{/if}
	            </div>
			</td>
			<td>{if condition="$value['isAudit'] == 1"}
				已审核
			{elseif condition="$value['isAudit'] == -1"}
				未审核
			{elseif condition="$value['isAudit'] == 2"}
				审核中
			{elseif condition="$value['isAudit'] == -2"}
				审核不通过
			{/if}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
				<a class="btn btn-default glyphicon glyphicon-pencil" href="{:url('User/toEdit',array('id' => $value['userId']))}">修改</a>&nbsp;
				<button type="button" class="btn btn-default glyphicon glyphicon-trash" onclick="del({$value['userId']},{$value['userType']});">刪除</button>&nbsp;
				<button style="margin-left: 10px;" type="button" class="btn btn-default glyphicon " onclick="window.location.href='{:url('admin/user/editMoney',array('userId'=>$value['userId']))}'">充值</button>
            </div>
			</td>
		</tr>
	<?php }?>
		
	</tbody>
</table>
{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
	//删除
   function del(id,type){
   		var msg = (type==1)?'该会员为商家会员，您确定要删除该商家信息吗？':'您确定要删除该会员信息吗?';
		layer.confirm(msg, {btn: ['取消', '确定'],btn2: function(index, layero){
		   //确定
			$.post("{:url('admin/User/del')}",{id:id},function(data){
				if(data.status=='1'){
					layer.msg(data.msg);
				}else{
					return false
				}
				},'json');
			location.reload();
			}
		}, function(index){
		  //取消
		  parent.layer.close(index);
		});
   }

   	//切换状态
	function toggleUserStatus(id,obj){
		var status = $(obj).attr('data-status');
		var id = id;
		var url = "{:url('admin/user/toggleUserStatus')}";
		$.post(url,{id:id,status:status},function(e){
			if(e.status==1){
				var message = e.state?'启用':'停用';
				var html = '<span data-status="'+e.state+'" onclick="toggleUserStatus('+e.id+',this)">'+message+'</span>';
				$("#data_"+e.id).html(html);
				layer.msg(e.msg);
			}else{
				layer.msg(e.msg);
				return false;
			}
		},'json')
	}

</script>
{/block}
