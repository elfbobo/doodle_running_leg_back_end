{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					余额操作
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal" name="editMoney" method="post" action="{:url('admin/user/editMoney')}" onSubmit="return checkForm()">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 余额：  </label>
						<div class="col-sm-10">
							<input type="hidden" name="userId" value="{$item['userId']}" class="col-xs-10 col-sm-4"/>
							<input type="hidden" name="userType" value="{$item['userType']}" class="col-xs-10 col-sm-4"/>
							<input type="hidden" name="userMoney"  id="userMoney" value="{$item['userMoney']}" class="col-xs-10 col-sm-4"/>
							<input type="text" value="{$item['userMoney']}" class="col-xs-10 col-sm-4" disabled="true" />
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 增加余额：  </label>
						<div class="col-sm-10">
							<input type="text" name="addMoney" id="addMoney" placeholder="增加的金额" value="" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 减少余额：  </label>
						<div class="col-sm-10">
							<input type="text" name="reduceMoney" id="reduceMoney" placeholder="减少的余额" value="" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<a class="btn" href="javascript:window.history.go(-1);">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								返回
							</a>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
function checkForm(){
	var money = $('#userMoney').val()
	var add = $('#addMoney').val()
	var reduce = $('#reduceMoney').val()
	if(add!=''){
	    if(isNaN(add))
	    {
	        layer.msg("不是数字类型!");
	        return false;
	    }
	}
	if(reduce!=''){
	    if(isNaN(reduce))
	    {
	        layer.msg("不是数字类型!");
	        return false;
	    }
	}
    if(add != '' && reduce!= '')
    {
        layer.msg('只能填写其中一项');
        return false;
    }else if( add == '' && reduce == ''){
    	layer.msg('不能同时为空');
        return false;
    }
    console.log(money+'-'+reduce)
   	if(parseInt(reduce)>parseInt(money)){
		layer.msg('减去金额过大')
		return false
	}
    return true;
}
</script>
{/block}