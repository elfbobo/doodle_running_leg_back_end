<?php
class File {
	
	/**
	 * 遍历文件夹下所有文件和目录
	 */
	public static function eachlist($path){
		static $lists = array();
		if (!is_dir($path)) return;
		$path = rtrim($path,DS);
		$dir = opendir($path);
		while (($read_dir = readdir($dir)) !== false){
			if ($read_dir != '.' && $read_dir != '..'){
				$sub_dir = $path.DIRECTORY_SEPARATOR.$read_dir;
				if (is_dir($sub_dir)){
					$lists[] = array('title' => '目录','type' => 'dir','path' => $sub_dir);
					self::eachlist($sub_dir);
				}else{
					$lists[] = array('title' => '文件','type' => 'file','file' => $read_dir,'path' => $sub_dir);
				}
			}
		}
		closedir($dir);
		return $lists;
	}
	
	public static function readable($dir = null){
		if (is_file($dir) && ($frst = file_get_contents($dir))) {
			return true; // 是文件，并且可读
		} else { // 是目录
			if (is_dir($dir) && scandir($dir)) {
				return true; // 目录可读
			} else {
				return false;
			}
		}
	}
	
	//文件夹和文件占用大小
	public static function dirSize($path){
		$dirSize = 0;
		if (!self::readable($path)) return;
		$path = rtrim($path,DS);
		$dir = opendir($path);
		while (($read_dir = readdir($dir)) !== false){
			if ($read_dir != '.' && $read_dir != '..'){
				$sub_dir = $path.DIRECTORY_SEPARATOR.$read_dir;
				if (is_dir($sub_dir)){
					$dirSize += self::dirSize($sub_dir);
				}else{
					$dirSize += filesize($sub_dir);
				}
			}
		}
		closedir($dir);
		return $dirSize;
	}
	
	//递归删除目录和文件
	public static function eachDelete($path){
		if (!self::readable($path)) return;
		$path = rtrim($path,DS);
		$dir = opendir($path);
		while (($read_dir = readdir($dir)) !== false) {
			if ($read_dir != '.' && $read_dir != '..'){
				$sub_dir = $path.DIRECTORY_SEPARATOR.$read_dir;
				if (is_dir($sub_dir)){
					self::eachDelete($sub_dir);
				}elseif (is_file($sub_dir)){
					unlink($sub_dir);
				}
			}
		}
		closedir($dir);
		if (!rmdir($path)) {
			//写日志
		}
	}
	
	/**
	 * @param unknown $filename
	 * @param unknown $writeText
	 * @param string $mode
	 * @return boolean
	 */
	public static function writeFile($filename,$writeText,$mode='wb'){
		if ($fp = @fopen($filename, $mode)){
			flock($fp, LOCK_EX);
			fwrite($fp, $writeText);
			flock($fp, LOCK_UN);
			fclose($fp);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * @param unknown $filename
	 * @param string $mode
	 * @return string|NULL
	 */
	public static function readFile($filename,$length=0,$mode='rb'){
		if ($fp = @fopen($filename, $mode)){
			flock($fp, LOCK_EX);
			if (!$length){
				$length = filesize($filename);
			}
			$content = fread($fp,$length);
			flock($fp, LOCK_UN);
			fclose($fp);
			return $content;
		}else{
			return null;
		}
	}
	
	/**
	 * @param $path
	 * @param int $property
	 * @return bool
	 */
	public static function makeDir($path, $property = 0777){
		return is_dir($path) or (self::makeDir(dirname($path), $property) and @mkdir($path, $property));
	}
	
	/**
	 * 功能：生成zip压缩文件
	 *
	 * @param $files        array   需要压缩的文件
	 * @param $filename     string  压缩后的zip文件名  包括zip后缀
	 * @param $path         string  文件所在目录
	 * @param $outDir       string  输出目录
	 *
	 * @return array
	 */
	public static function zip($files, $filename, $outDir = ROOT_PATH, $path = ROOT_PATH){
		$zip = new \ZipArchive();
		File::makeDir($outDir);
		$res = $zip->open($outDir . $filename, \ZipArchive::CREATE);
		if ($res) {
			foreach ($files as $file) {
				if (empty($file)) {
					continue;
				}
				if ($t = $zip->addFile($path . $file, str_replace('/', '', $file))) {
					$t = $zip->addFile($path . $file, str_replace('/', '', $file));
				}
			}
			$zip->close();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 功能：解压缩zip文件
	 *
	 * @param $file         string   需要压缩的文件
	 * @param $outDir       string   解压文件存放目录
	 *
	 * @return array
	 */
	public static function unzip($file, $path = ROOT_PATH, $outDir = ROOT_PATH){
		$zip = new \ZipArchive();
		if ($zip->open($path.$file) !== true) return false;
		$zip->extractTo($outDir);
		$zip->close();
		return true;
	}
	
	public static function byteFormat($bytes){
		$size_text = array(" B", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		if ($bytes == 0) return '0KB';
		$i = floor(log($bytes, 1024));
		return round($bytes / pow(1024, $i), 2) . $size_text[$i];
	}
	
}